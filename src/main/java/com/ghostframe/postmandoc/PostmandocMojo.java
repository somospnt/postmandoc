/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ghostframe.postmandoc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ghostframe.postmandoc.postman.PostmanCollectionFactory;
import com.ghostframe.postmandoc.postman.domain.PostmanCollection;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "generate", defaultPhase = LifecyclePhase.PREPARE_PACKAGE)
public class PostmandocMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project.build.directory}/generated-snippets/")
    private String generatedSnippetsDirectory;
    @Parameter
    private String replacementHost;
    @Parameter(defaultValue = "${project.build.directory}/${project.name}.postman_collection.json")
    private File outputFile;
    @Parameter(defaultValue = "${project.name}")
    private String collectionName;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        try {
            PostmanCollection postmanCollection = PostmanCollectionFactory.fromSnippetsFolder(collectionName, new File(generatedSnippetsDirectory), replacementHost);
            String collectionJson = writeAsJson(postmanCollection);
            FileUtils.write(outputFile, collectionJson, StandardCharsets.UTF_8);
            getLog().info("Generated Postman collection: " + outputFile);
        } catch (IOException ex) {
            getLog().error(ex);
        }
    }

    private String writeAsJson(Object object) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(object);
    }

    public String getGeneratedSnippetsDirectory() {
        return generatedSnippetsDirectory;
    }

    public void setGeneratedSnippetsDirectory(String generatedSnippetsDirectory) {
        this.generatedSnippetsDirectory = generatedSnippetsDirectory;
    }

    public File getOutputFile() {
        return outputFile;
    }

    public void setOutputFile(File outputFile) {
        this.outputFile = outputFile;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }
}
