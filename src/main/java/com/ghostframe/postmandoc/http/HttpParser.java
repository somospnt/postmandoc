/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ghostframe.postmandoc.http;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.impl.io.DefaultHttpRequestParser;
import org.apache.http.impl.io.HttpTransportMetricsImpl;
import org.apache.http.impl.io.SessionInputBufferImpl;

public class HttpParser {

    public static HttpRequest parse(String httpRequestText) throws IOException, HttpException {
        try {
            return new DefaultHttpRequestParser(createSessionInputBuffer(httpRequestText)).parse();
        } catch (HttpException | IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private static SessionInputBufferImpl createSessionInputBuffer(String string) {
        SessionInputBufferImpl sessionInputBuffer = new SessionInputBufferImpl(new HttpTransportMetricsImpl(), 255);
        sessionInputBuffer.bind(IOUtils.toInputStream(string, StandardCharsets.UTF_8));
        return sessionInputBuffer;
    }

}
